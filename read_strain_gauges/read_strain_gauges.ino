/*
 *  Script to read+print analogue values from strain gauges
 */

#include <Mouse.h>


#define AXIS_0 A0 // Left-right
#define AXIS_1 A1 // Up-down
#define PIN_MOUSE_ENABLE 9
#define PIN_MOUSE_L 3 // Left
#define PIN_MOUSE_R 2 // Right
#define PIN_MOUSE_M 4 // Middle


float baseline0 = 0, baseline1=0;
bool enabled, was_enabled;
bool mouse_L, mouse_L_was, mouse_R, mouse_R_was, mouse_M, mouse_M_was;


void setup() {
  pinMode(AXIS_0, INPUT);
  pinMode(AXIS_1, INPUT);
  pinMode(PIN_MOUSE_ENABLE, INPUT_PULLUP);
  pinMode(PIN_MOUSE_L, INPUT_PULLUP);
  pinMode(PIN_MOUSE_R, INPUT_PULLUP);
  pinMode(PIN_MOUSE_M, INPUT_PULLUP);

  Serial.begin(115200);

  recalibrate();
}


void loop() {
  enabled = !digitalRead(PIN_MOUSE_ENABLE);

  // Recalibrate on fresh enable connection
  if (enabled && !was_enabled) {
    recalibrate();
    // Connect as a mouse
    Serial.println("Begin mouse");
    Mouse.begin();
  }
  else if (was_enabled && !enabled) {
    // Close mouse connection
    Serial.println("End mouse");
    Mouse.end();
  }

  // Only do mouse stuff when enabled
  if (enabled) {
    moveMouse();
    handleMouseClicks();
  }

  was_enabled = enabled;
}


float readAverage(uint8_t axis, uint16_t N) {
  /*
   *  Average N readings from sensor
   */

  float average=0;

  for (uint16_t i=0; i<N; i++) {
    average += (float)analogRead(axis) / N;
  }

  return average;
}


void recalibrate() {
  // Give me a second get my hands away
  baseline0 = readAverage(AXIS_0, 1000);
  baseline1 = readAverage(AXIS_1, 1000);
  delay(1000);
  // Average readings to get a baseline0 (not sure why there's an offset of 2)
  baseline0 = readAverage(AXIS_0, 1000) + 2;
  baseline1 = readAverage(AXIS_1, 1000) + 2;
}


void moveMouse() {
  /*
   *  Find amount to move mouse and move it
   */

  static float reading0, reading1, difference0, difference1;

  reading0 = readAverage(AXIS_0, 25);
  reading1 = readAverage(AXIS_1, 25);
  //Serial.print(reading0);
  //Serial.print(",");
  //Serial.println(reading1);

  difference0 = reading0 - baseline0;
  difference1 = reading1 - baseline1;

  //Serial.print(difference0);
  //Serial.print(",");
  //Serial.println(difference1);

  moveMouse(difference0, difference1);
}


void handleMouseClicks() {
  /*
   *  Handle left, middle and right clicks
   */

  // Get button states
  mouse_L = !digitalRead(PIN_MOUSE_L);
  mouse_R = !digitalRead(PIN_MOUSE_R);
  mouse_M = !digitalRead(PIN_MOUSE_M);

  // Check for new presses or releases
  if (mouse_L != mouse_L_was) {
    if (mouse_L) Mouse.press(MOUSE_LEFT);
    else Mouse.release(MOUSE_LEFT);
  }
  if (mouse_R != mouse_R_was) {
    if (mouse_R) Mouse.press(MOUSE_RIGHT);
    else Mouse.release(MOUSE_RIGHT);
  }
  if (mouse_M != mouse_M_was) {
    if (mouse_M) Mouse.press(MOUSE_MIDDLE);
    else Mouse.release(MOUSE_MIDDLE);
  }

  // Save button states for next time
  mouse_L_was = mouse_L;
  mouse_R_was = mouse_R;
  mouse_M_was = mouse_M;
}


void moveMouse(float difference0, float difference1) {
  // Map to how much to move
  int16_t input_range = 350; // Radius
  int16_t input_deadzone = 50; // Radius
  int8_t range = 20; // Maximum speed
  int8_t move_x, move_y;

  if (abs(difference0) < input_deadzone)
    move_x = 0;
  else {
    if (difference0 > 0) difference0 -= input_deadzone;
    else difference0 += input_deadzone;
    move_x = map((int16_t) difference0, -input_range, input_range, -range, range);
  }

  if (abs(difference1) < input_deadzone)
    move_y = 0;
  else {
    if (difference1 > 1) difference1 -= input_deadzone;
    else difference1 += input_deadzone;
    move_y = -map((int16_t) difference1, -input_range, input_range, -range, range);
  }

  Serial.print(move_x);
  Serial.print(",");
  Serial.println(move_y);
  Mouse.move(move_x, move_y);
}
